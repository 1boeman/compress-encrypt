#!/bin/env bash
# run as root 

start=$(date +%s)

export GPG_TTY=$(tty)
cd "$(dirname "$0")"
source config.sh

echo "$SOURCE"
echo "$TAR_DESTINATION"

tar -czvf "$TAR_DESTINATION" "$SOURCE"

gpg --batch --yes --status-file /tmp/gpg_status.txt --with-colons --pinentry-mode=loopback --cipher-algo AES256 --passphrase-file "$PASSWORD_FILE" -c "$TAR_DESTINATION"

rm "$TAR_DESTINATION"

rsync -avz --progress "$TAR_DESTINATION.gpg" $BACKUP_DIR

end=$(date +%s)
echo "Elapsed Time: $(($end-$start)) seconds"
